import Vue from 'vue'
import VueMaterial from 'vue-material'
import Application from './Application'
import Sidebar from './components/Sidebar'
import Grid from './components/Grid'


Vue.use(VueMaterial)

Vue.material.registerTheme('default', {
	primary:	'indigo',
	accent:		'pink',
	warn:		'red',
	background: 'grey'
})

Vue.component('ai-sidebar', Sidebar)
Vue.component('ai-grid', Grid)

new Vue({
	el: '#application',
	template: '<Application/>',
	components: { Application }
})
