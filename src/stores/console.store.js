class Console {
	constructor () {
		this.data = ''
	}

	log () {
		this.data += `${Array.prototype.join.call(arguments, ' ')}\n`

		return this
	}

	reset () {
		this.data = ''

		return this
	}
}

export default Console
