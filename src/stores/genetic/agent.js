import { random, randomPosition, randomOrientation, randomTable } from './utils'


const sensorOrientations = [
	[7, 6, 5, 4, 3, 2, 1, 0],
	[1, 0, 7, 6, 5, 4, 3, 2],
	[3, 2, 1, 0, 7, 6, 5, 4],
	[5, 4, 3, 2, 1, 0, 7, 6]
]

class Agent {
	constructor (grid, agent) {
		grid = grid || agent._grid

		this._grid		= grid
		this._counts	= {
			wall:		0,
			stucked:	0,
			left:		0,
			right:		0
		}

		this.orientation	= randomOrientation()
		this.position		= randomPosition(grid)
		this.table			= agent ? agent.table.slice() : randomTable()
		this.fitness		= agent ? agent.fitness : 0

		if (!agent) this._fitness()
	}

	move () {
		const { orientation }	= this
		const grid				= this._grid
		const percept			= this._sense()
		const index				= 2 * percept
		const action			= this.table[(index / 32) >> 0] >> (index & 31) & 3

		switch (action) {
			case 0:
				const offset	= orientation & 1 ? grid.columns : 1
				let nextIndex	= this.position

				nextIndex += orientation & 2 ? -offset : offset
				if (grid.data[nextIndex] === 0) this.position = nextIndex

				break
			case 1:
				this.orientation = orientation === 3 ? 0 : orientation + 1
				break
			case 2:
				this.orientation = orientation > 1 ? orientation - 2 : orientation + 2
				break
			case 3:
				this.orientation = orientation === 0 ? 3 : orientation - 1
				break
		}

		return [percept, action]
	}

	_sense () {
		const { position }		= this
		const { data, columns }	= this._grid
		const indexTop			= position - columns
		const indexBottom		= position + columns

		let sensorOrientation = sensorOrientations[this.orientation]

		return data[indexTop]		<< sensorOrientation[0]
			| data[indexTop + 1]	<< sensorOrientation[1]
			| data[position + 1]	<< sensorOrientation[2]
			| data[indexBottom + 1]	<< sensorOrientation[3]
			| data[indexBottom]		<< sensorOrientation[4]
			| data[indexBottom - 1]	<< sensorOrientation[5]
			| data[position - 1]	<< sensorOrientation[6]
			| data[indexTop - 1]	<< sensorOrientation[7]
	}

	_fitness () {
		const iterations	= 5
		const counts		= this._counts
		const grid			= this._grid
		const { length }	= grid

		for (let i = 0; i < iterations; i++) {
			let moves = length * 5

			while (--moves) {
				const { position, orientation } = this

				let [percept, action] = this.move()

				if (this.position === position) {
					if (action === 2 || this.orientation === orientation) counts.stucked++
					else if (action === 1) counts.right++
					else if (action === 3) counts.left++
					continue
				}

				if (percept > 0) counts.wall++
			}

			this.position = randomPosition(grid)
		}

		this.fitness	= counts.wall - counts.stucked - counts.left - 2 * counts.right
		counts.right	= counts.left = counts.wall = counts.stucked = 0

		return this.fitness
	}
}

export default Agent
