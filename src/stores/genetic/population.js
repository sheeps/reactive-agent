import { random } from './utils'
import Agent from './agent'


const onePointCrossover = (father, mother) => {
	const fatherTable	= father.table
	const motherTable	= mother.table
	const rand			= 1 + random(511)
	const offset		= rand / 32 >> 0
	const power			= 1 << (rand & 31)
	const offspring1	= new Uint32Array(16)
	const offspring2	= new Uint32Array(16)

	for (let j = 0; j < 16; j++) {
		const fatherTableLine	= fatherTable[j]
		const motherTableLine	= motherTable[j]

		let mask1, mask2

		if (j === offset) {
			mask1 = 4294967296 - power
			mask2 = power - 1
		} else if (j > offset) {
			mask1 = 0
			mask2 = 4294967295
		} else {
			mask1 = 4294967295
			mask2 = 0
		}

		offspring1[j] = (fatherTableLine & mask1) | (motherTableLine & mask2)
		offspring2[j] = (fatherTableLine & mask2) | (motherTableLine & mask1)
	}

	father.table = offspring1
	mother.table = offspring2

	father._fitness()
	mother._fitness()
}

class Population {
	constructor (grid, console, length, crossover, mutation) {
		const agents	= new Array(length)

		let fittest = null

		this.console	= console
		this.ready		= false
		this.generation	= 0
		this.length		= length
		this.agents		= agents
		this.mutation	= mutation
		this.size		= {
			crossover:	(crossover * length + .5) >> 0,
			copy:		((1 - crossover) * length + .5) >> 0
		}

		fittest = agents[0] = new Agent(grid)

		for (let i = 1; i < length; i++) {
			const agent = agents[i] = new Agent(grid)

			if (agent.fitness > fittest.fitness) fittest = agent
		}

		this.fittest = fittest

		console
			.log(`The population contains ${length} individuals`)
			.log(`The fittest has been evaluated with a score of ${fittest.fitness}`)
	}

	evolve () {
		const { length }	= this
		const agents		= new Array(length)

		this._copy(agents)
		this._crossover(agents)

		this.agents = agents
		this.generation++

		this._mutate()

		this.console
			.log('---')
			.log('Generation:', this.generation)
			.log('Fittest individual:', this.agents.indexOf(this.fittest))
			.log('Score of the fittest:', this.fittest.fitness)

		return this.fittest
	}

	_select (n, m) {
		const { agents, length }	= this
		const selected				= new Array(n)

		let index = 0

		while (index < n) {
			let fittest = agents[random(length)]

			for (let i = 1; i < m; i++) {
				const agent = agents[random(length)]

				if (agent.fitness > fittest.fitness) fittest = agent
			}

			selected[index++] = new Agent(null, fittest)
		}

		return selected
	}

	_copy (agents) {
		const { copy }	= this.size
		const selected	= this._select(copy, 5)

		let fittest = agents[0] = selected[0]

		for (let i = 1; i < copy; i++) {
			const agent = agents[i] = selected[i]

			if (agent.fitness > fittest.fitness) fittest = agent
		}

		this.fittest = fittest
	}

	_crossover (agents) {
		const { size }		= this
		const { crossover }	= size
		const selected		= this._select(crossover, 5)

		let index		= size.copy
		let { fittest }	= this

		for (let i = 0; i < crossover; i += 2) {
			const father	= selected[i]
			const mother	= selected[i + 1 < crossover ? i + 1 : 0]

			onePointCrossover(father, mother)

			agents[index++]	= father
			agents[index++]	= mother

			if (father.fitness > mother.fitness)
				if (father.fitness > fittest.fitness) fittest = father
			else if (mother.fitness > fittest.fitness) fittest = mother
		}

		this.fittest = fittest
	}

	_mutate () {
		const { length, agents, mutation, console } = this

		let { fittest } = this

		for (let i = 0; i < length; i++) {
			if (Math.random() > mutation) continue

			const offset	= random(15)
			const index		= random(31)
			const agent		= agents[i]

			agent.table[offset] ^= 1 << index

			if (agent._fitness() > fittest.fitness)

			console.log(`Individual ${i} has been mutated`)
		}

		this.fittest = fittest
	}
}

export default Population
