import * as utils from './utils'
import Agent from './agent'
import Population from './population'

export default { utils, Agent, Population }
