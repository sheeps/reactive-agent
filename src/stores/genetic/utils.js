export const random = n => {
	const rand = Math.random() * n >> 0

	return rand === n ? random() : rand
}

export const randomOrientation = () => random(4)

export const randomTable = () => {
	const table = new Uint32Array(16)

	for (let i = 0; i < 16; i++) table[i] = random(4294967295)

	return table
}

export const randomPosition = grid => {
	const { data, columns }	= grid
	const position			= random(grid.length)

	return data[position]
		|| data[position + 1]
		|| data[position - 1]
		|| data[position - columns]
		|| data[position + columns] ? randomPosition(grid) : position
}
