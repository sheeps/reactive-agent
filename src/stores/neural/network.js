import Console from '../console.store'
import { sigmoidPrime } from './utils'
import Layer from './layer'


class Network {
	constructor (lengths, alpha) {
		const size		= lengths.length
		const layers	= new Array(size - 1)

		this.size		= size
		this.layers		= layers
		this.alpha		= alpha
		this.console	= new Console()

		for (let i = 1; i < size; i++) layers[i] = new Layer(lengths[i], lengths[i - 1])
	}

	train (examples, threshold) {
		const { layers, size } = this

		let epochs = 0
		let errors

		do {
			errors = 0

			for (let i = 0, length = examples.length; i < length; i++) {
				const example = examples[i]

				this.activate(example.inputs)
				errors += this.update(example)
			}

			if (++epochs > 50000) break
		} while (errors > threshold)

		return epochs
	}

	activate (inputs) {
		const { layers } = this

		let outputs = layers[1].activate(inputs)

		for (let i = 2, size = this.size; i < size; i++) outputs = layers[i].activate(outputs)
	}

	update ({ inputs, target}) {
		const { layers, alpha }	= this
		const outputLayerTlus	= layers[this.size - 1].tlus
		const hiddenLayerTlus	= layers[this.size - 2].tlus
		const hiddenLength		= hiddenLayerTlus.length
		const inputsLength		= inputs.length
		const deltas			= new Float32Array(hiddenLength)

		let errors = 0

		for (let i = 0, outputLength = outputLayerTlus.length; i < outputLength; i++) {
			const { input, output, weights }	= outputLayerTlus[i]
			const error							= target[i] - output
			const delta							= sigmoidPrime(input) * error

			errors += error * error

			weights[0] += alpha * delta

			for (let j = 0; j < hiddenLength; j++) {
				const weight = weights[j + 1] += alpha * hiddenLayerTlus[j].output * delta

				deltas[j] += weight * delta
			}
		}

		for (let j = 0; j < hiddenLength; j++) {
			const { input, weights }	= hiddenLayerTlus[j]
			const delta					= sigmoidPrime(input) * deltas[j]

			weights[0] += alpha * delta

			for (let k = 0; k < inputsLength; k++) weights[k + 1] += alpha * inputs[k] * delta
		}

		return errors
	}
}

export default Network
