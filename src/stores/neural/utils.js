export const sigmoid		= x => 1 / (1 + Math.exp(-x))
export const sigmoidPrime	= x => sigmoid(x) * (1 - sigmoid(x))
