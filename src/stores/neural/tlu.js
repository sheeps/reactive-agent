import { sigmoid } from './utils'


class TLU {
	constructor (size) {
		const weights = new Float32Array(++size)

		this.size		= size
		this.weights	= weights
		this.input		= null
		this.output		= null

		for (let i = 0; i < size; i++) weights[i] = this._randomWeight()
	}

	activate (inputs) {
		const { weights, size } = this

		let weightedSum = weights[0]

		for (let i = 1; i < size; i++) weightedSum += inputs[i - 1] * weights[i]

		const output = sigmoid(weightedSum)

		this.input	= weightedSum
		this.output	= output

		return output
	}

	_randomWeight () {
		return Math.random() - .5
	}
}

export default TLU
