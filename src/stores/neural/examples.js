export default [{
	inputs: [0, 0],
	target: [0.01]
}, {
	inputs: [0, 1],
	target: [0.99]
}, {
	inputs: [1, 0],
	target: [0.99]
}, {
	inputs: [1, 1],
	target: [0.01]
}]
