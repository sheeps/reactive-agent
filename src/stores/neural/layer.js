import TLU from './tlu'


class Layer {
	constructor (size, inputsSize) {
		const tlus = new Array(size)

		this.size	= size
		this.tlus	= tlus

		for (let i = 0; i < size; i++) tlus[i] = new TLU(inputsSize)
	}

	activate (inputs) {
		const { size, tlus }	= this
		const outputs			= new Float32Array(size)

		for (let i = 0; i < size; i++) outputs[i] = tlus[i].activate(inputs)

		return outputs
	}
}

export default Layer
