class Grid {
	constructor (lines, columns) {
		const length		= lines * columns
		const data			= new Uint8Array(length)
		const lastLineIndex	= lines - 1
		const lastRowIndex	= columns - 1

		this.lines		= lines
		this.columns	= columns
		this.data		= data
		this.length		= length

		let index = 0

		for (let j = 0; j < columns; j++) data[index++] = 1

		for (let i = 1; i < lastLineIndex; i++) {
			data[index] = 1
			data[index + lastRowIndex] = 1
			index += columns
		}

		for (let j = 0; j < columns; j++) data[index++] = 1
	}

	level1 () {
		const { data, lines, columns } = this

		const line	= 1 + random(lines / 2 >> 0)
		const row	= 1 + random(columns / 2 >> 0)

		let index = columns + 1

		for (let i = 0; i < line; i++) {
			for (let j = 0; j < row; j++) data[index + j] = 1

			index += columns
		}

		return this
	}
}

export default Grid
