# Reactive agent

[![build status](https://gitlab.com/sheeps/reactive-agent/badges/master/build.svg)](https://gitlab.com/sheeps/reactive-agent/commits/master)

> This project gives some basic implementations in Javascript of a few machine learning concepts used for the design of a reactive agent.

Each example consists in training a reactive agent to walk along the wall of a room.

The room is a rectangular grid divided into equal-sized squares, called cells, surrounded by bricks (in black) which form the wall.

Our agent can only face 4 directions and perform the following 4 actions :

* Move forward
* Turn right
* Turn left
* Turn around

It also has 8 sensors that can detect the existence of a wall in its 8 neighboring cells.

As a reactive agent, it will only choose its action given the current percept. Our goal is to improve its response to a percept using these methods:

* [Genetic algorithm](https://sheeps.gitlab.io/reactive-agent)
* _Reinforcement learning (WIP)_
* _Artificial neural network (WIP)_



____


*Following the course of [Derek Bridge](http://www.cs.ucc.ie/~dgb/index.html) on Intelligence Artificial 1 at [UCC](https://www.ucc.ie/en "University College Cork").*
